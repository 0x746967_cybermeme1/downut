#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 14:57:39 2023

@author: cybermime

downut is my bug bounty recon tool.
That tool is in constant development.
It does fits my needs based on my background. 
If it fits yours, enjoy. If you are looking for a bug bounty recon tool, I recommand Nuclei.
"""

'''
BUG to Fix:

    
'''

### IMPORTS ###
import argparse
import configparser
#import json
#import ssl
#import sys

from datetime import datetime
from tabulate import tabulate

from src.crawler.crawler import crawl_comments
from src.internals.api_connect import wayback_api_list
#from src.internals.sqlite3_db import check_tables
from src.transformations.check_whois import IP2AS
from src.transformations.dns_tools import dns_check
from src.transformations.domain import get_domain_name, get_sub_domain_name
#from src.transformations.files_stuff import file_md5
#from src.transformations.rdap import rdap_print_nice
from src.transformations.seo_tools import robot_hunting, pretify_robot_result
from src.transformations.sort_functions import wayback_machine_hash_sort
from src.transformations.wildcard_domain_auto_recon import cert_check_hunt
from src.pretify.pretify import line_separator


### ARGPARSE PREP ###
parser = argparse.ArgumentParser()

parser.add_argument('-A', "--all", help='Tous les Tests' , action='store_true')
parser.add_argument('-c', "--comments", help='recherche les commentaires', action='store_true')
parser.add_argument('-cj', "--commentsjs", help='imprime a l\'ecran les commentaires des fichiers JS' , action='store_true')
parser.add_argument('-C', "--certs", help='fait du certificat transparancy sur crt.sh', action='store_true')
parser.add_argument('-d', "--delay", help='delay entres requetes', default=2000) # en ms donc 2 s
parser.add_argument('-db', "--date_begin", help='Date de debut de recherche', default=2000)
parser.add_argument('-de', "--date_end", help='Date de fin de recherche', default=int(str(datetime.now())[:4]) ) # default = now, l'annee en cours
parser.add_argument('-D', "--dns", help='recupere les infos relatives aux DNS' , action='store_true')
parser.add_argument('-r', "--robots", help="collecte  et analyse le fichier robots.txt" , action='store_true')
parser.add_argument('-mt', "--waybackmachine_sort_by_time", help='recupere les dates des snaps de waybackmachine' , action='store_true')
parser.add_argument('-mh', "--waybackmachine_sort_by_hashes", help='recupere les dates des snaps de waybackmachine et les sorts par hash' , action='store_true')
parser.add_argument('-o', "--output", help='fichier de sortie pour les informations des JS' , default=None)
parser.add_argument('-u', "--url", help="l'url de la cible" , required=True)
parser.add_argument('-ua', "--useragent", help='Le user-agent a utiliser' , required=True)
parser.add_argument('-W', "--whois", help='recupere les infos relatives aux whois' , action='store_true')

args = parser.parse_args()

DATE_BEGN = args.date_begin
DATE_END = args.date_end
DELAY_REQUETES = args.delay
SAVE_FILE = args.output
USER_AGENT = args.useragent

CERTS = args.certs
DNS = args.dns
FULL_COMMENTS = args.comments
PRINT_COMMENTS = args.commentsjs
ROBOTS = args.robots
URL = args.url
WAYBACK_MACHINE = args.waybackmachine_sort_by_time
WAYBACK_MACHINE_SORTED = args.waybackmachine_sort_by_hashes
WHOIS = args.whois

if not URL.startswith("http"):
    URL = "https://" + URL

if args.all:
    WHOIS = True
    DNS = True
    ROBOTS = True
    CERTS = True
    PRINT_COMMENTS = True
    FULL_COMMENTS = True
    WAYBACK_MACHINE = True
    WAYBACK_MACHINE_SORTED = True


### VARIABLE AND CONSTANTS ###
config_filename = 'downut.ini'
ipv4 = []
ipv6 = [] 


### COLLECT FROM CONFIG FILE ###
config_from_file = configparser.ConfigParser()
config_from_file.read(config_filename)

db_name = config_from_file['sqlite3']['db_name']
builtwith_api_key = config_from_file['API']['builtwith']


### MAIN ###
if __name__ == "__main__":
    
    ### TODO BEGIN
    #check_tables(db_name)
    ### TODO END
    
    ## Extract domain from url
    domain_name_collected = get_domain_name(URL)
    subdomain_name_collected = get_sub_domain_name(URL)
    
    # Collect DNS information
    if DNS:
        dns_results = dns_check(subdomain_name_collected)
    else:
        dns_results = None
    
    # Collect robots.txt file
    if ROBOTS:
        robots_trouve = robot_hunting(URL, USER_AGENT)
    else: robots_trouve = False
    
    
    #Check DNS results
    if dns_results:   
        print("\nDNS records:")
        print("============")
        print('\n', tabulate(dns_results, headers=['URL','Records','Value']))
        for i in dns_results:
            #Collect ipv4 and 6
            if i[1] == 'A':
                ipv4.append(i[2])
            if i[1] == 'AAAA':
                ipv6.append(i[2])
        
        line_separator()

        # No needed anymore
        '''print("\nASN List:")
        print("=========")
        if ipv4:
            rdap_print_nice(ipv4)
        if ipv6:
            rdap_print_nice(ipv6)
        
        line_separator()'''


    if WHOIS:
        print("\nWhois List:")
        print("============\n")
        print(tabulate(IP2AS(subdomain_name_collected), headers=['AS','IP','AS Name']))
        
        line_separator()    


    if CERTS:
        print("\nCerts List:")
        print("============\n")
        cert_check_hunt([domain_name_collected])
    
        line_separator()
    

    #If data in robots.txt
    if robots_trouve:
        print("\nRobot.txt:")
        print("==========")
        liste_disallowed_link = pretify_robot_result(robots_trouve, URL, True)
    
        line_separator()

        if liste_disallowed_link:    
            print("\nDisallowed links:")
            print("=================")
            for i in liste_disallowed_link:
                print(i)
    
            line_separator()


    #If comments are requested
    if FULL_COMMENTS:
        print("\nComments:")
        print("=========")    
        results = crawl_comments([URL], DELAY_REQUETES, USER_AGENT, SAVE_FILE, PRINT_COMMENTS) # 2000 = 2sec min
        print('\n', tabulate(results, headers=['URL','Ind','comment']))

        line_separator()


    #Check into wayback machine and sort results based on timestamp or Hashes
    if WAYBACK_MACHINE or WAYBACK_MACHINE_SORTED:
        print("\nWayback machine snapshots:")
        print("==========================")
        results = wayback_api_list(URL, USER_AGENT, DATE_BEGN, DATE_END)

        if WAYBACK_MACHINE:
            print("\n Sorted by Timestamp")
            print('\n', tabulate(results, headers=['DOMAIN', 'URL', 'TIMESTAMP' , 'SC', 'LENGTH', 'HASH']))

        if WAYBACK_MACHINE_SORTED:
            print("\n Sorted by Hashes")
            print('\n', tabulate(wayback_machine_hash_sort(results), headers=['DOMAIN', 'URL', 'TIMESTAMP' , 'SC', 'LENGTH', 'HASH']))

        line_separator()       
