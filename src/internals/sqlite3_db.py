#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 16:01:53 2023

@author: cybermime

TODO:
    ALL
"""

### IMPORTS ###
import sqlite3


### VARIABLES ###
tables_values = {
    'rdap':'(domain, fqdn, IP, subnet, ASN, AS_name, AS_country)',
    'domain':'(domain, fqdn, issuer_name, min_entry_timestamp, expire_date, cert_name ,list_domain_certs, perime)',
    'comments':'(fqdn, Index_in_page, comment)',
    'files_check': '(filename, hash_md5)',
    }


def generate_tables(db_name, table_name):
    '''
    Generate tables that have to be in sqlite3 DB.
    '''
    sqlite3_con = sqlite3.connect(db_name)
    sqlite3_cursor = sqlite3_con.cursor()
    
    sqlite3_cursor.execute("CREATE TABLE "+ table_name + tables_values[table_name])
    sqlite3_con.close()
    

def check_presence_tables(db_name):
    '''
    Check if the requiered tables are in sqlite3 DB
    '''
    sqlite3_con = sqlite3.connect(db_name)
    sqlite3_cursor = sqlite3_con.cursor()
    
    res = sqlite3_cursor.execute("SELECT name FROM sqlite_master")
    resultats = res.fetchall()
    sqlite3_con.close()
    
    return resultats
    

def check_tables(db_name):
    '''
    Check if the requiered tables are in sqlite3 DB -> Create missing
    '''
    tables_checked = check_presence_tables(db_name)
    if not tables_checked:
        for i in tables_values.keys():
            generate_tables(db_name,i)
    else:
        for i in tables_checked:
            if not i[0] in tables_values:
                generate_tables(db_name,i[0])


def request_tables(db_name,request):
    '''
    SQL Request and return result
    
    Return:
        SQL request result
    '''
    sqlite3_con = sqlite3.connect(db_name)
    sqlite3_cursor = sqlite3_con.cursor()
    
    #exemple request "SELECT name FROM sqlite_master WHERE name='spam'"
    # DO NOT PROD: SQLi
    res = sqlite3_cursor.execute(request)
    
    return res.fetchall()


def insert_tables(db_name,request):
    '''
      Send an SQL request
      
      Return:
          0 -> ok
          1 -> error
    '''
    sqlite3_con = sqlite3.connect(db_name)
    sqlite3_cursor = sqlite3_con.cursor()

    #exemple request "INSERT into movie VALUES 
    #    ('Monty Python and the Holy Grail', 1975, 8.2),
    #    ('And Now for Something Completely Different', 1971, 7.5)"
    # DO NOT PROD: SQLi
    try:
        sqlite3_cursor.execute(request)
        sqlite3_con.commit()
        sqlite3_con.close()
        return 0
    
    except:
        sqlite3_con.close()
        return 1
                