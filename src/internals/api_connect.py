#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 15:59:40 2024

@author: cybermime
"""

### IMPORTS ###
import json,urllib.request
from waybackpy import WaybackMachineSaveAPI, WaybackMachineCDXServerAPI


### FUNCTIONS ###

### TODO BEGIN ###
def builtwith_api(DOMAIN, buildwith_api_key):
    '''
    Expected result:
        Extracts data from Builtwith API
        
    TODO:
        has to work on it
    '''
    requete_json = "https://api.builtwith.com/free1/api.json?KEY=" + buildwith_api_key + "&LOOKUP=" + DOMAIN
        
    data = urllib.request.urlopen(requete_json).read()
    output = json.loads(data)
    
    print("Domain", output["domain"])
    
    for i in output["groups"]:
        for j in i['categories']:
            print(i['name'], j['name'])
### TODO END ###


def wayback_api(DOMAIN, USER_AGENT):
    ''' 
    Print the last snapshot in waybackmachine based on the provided url
    '''
    save_api = WaybackMachineSaveAPI(DOMAIN, USER_AGENT)
    last_url_saved = save_api.save()
    last_cache_saved = save_api.cached_save
    last_timestamp = save_api.timestamp()
    
    print("Last wayback = {}, is cached : {} backup Timestamp: {}".format(last_url_saved, last_cache_saved, last_timestamp))


def wayback_api_list(DOMAIN, USER_AGENT, DATE_BEGN, DATE_END):
    ''' 
    Collect list of snapshot in waybackmachine based on the provided url
    
    Return:
        List
    '''
    cdx = WaybackMachineCDXServerAPI(DOMAIN, USER_AGENT, start_timestamp=DATE_BEGN, end_timestamp=DATE_END)
    liste_return = []
    
    for item in cdx.snapshots():
        #Transforming Timestamp
        ### TODO BEGIN ###
        ### Use datetime function ! ###
        date_str = str(item.timestamp)[:4] + '-' + str(item.timestamp)[4:6] + '-' + str(item.timestamp)[6:8] + \
            ' ' + str(item.timestamp)[8:10] + ':' + str(item.timestamp)[10:12] + ':' + str(item.timestamp)[12:]
        ### TODO END ###
        
        liste_return.append([DOMAIN, item.archive_url, date_str , item.statuscode, item.length , item.digest])
    
    return liste_return
