#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 17:17:32 2024

@author: cybermime
"""
### IMPORTS ###
from operator import itemgetter


### FUNCTIONS ###
def wayback_machine_hash_sort(liste_en_entree):
    '''
    Sort the wayback results based on the 6th field (sort by Hashes)
    
    Entry list:
        ['DOMAIN', 'URL', 'TIMESTAMP' , 'SC', 'LENGTH', 'HASH']
    
    Return:
        List
    '''
    sorted_list = sorted(liste_en_entree, key=itemgetter(5))
    return sorted_list