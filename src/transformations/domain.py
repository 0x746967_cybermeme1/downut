"""
Created on --

@author: cybermime
"""
### IMPORTS ###
from urllib.parse import urlparse


### FUNCTIONS ###
def get_domain_name(url):
    '''  
    get domainname ( extract only the 2 last parts eg: foo.bar)
    
    Return:
        String
    '''
    try:
        results = get_sub_domain_name(url).split('.')
        return results[-2] + '.' + results[-1]
    except:
        return ""


# on recupere le subdomain (fqdn)
def get_sub_domain_name(url):
    ''' 
    extract url
    
    Return:
        String
    '''
    try:
        return urlparse(url).netloc
    except:
        return ""