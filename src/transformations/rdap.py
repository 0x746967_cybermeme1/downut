#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 15:38:53 2023

@author: cybermime

"""
### IMPORTS ###
from ipwhois import IPWhois
from tabulate import tabulate

from ..internals.sqlite3_db import request_tables


### VARIABLES ###
results = []


### FUNCTIONS ###
def rdap_lookup(ip_to_check):
    '''
    Entry is IP and return information relative to it
    
    Returned Table:
        [IP, Subnet, numero d'AS, nom de l'AS, Pays de l'AS]

    Return:
        List
    '''
    for ip_in_list in ip_to_check:
        obj = IPWhois(ip_in_list)
        result=(obj.lookup_rdap(depth=1))
        results.append([ip_in_list,result['asn_cidr'],result['asn'],result['asn_description'],result['asn_country_code']])
    return results


def rdap_print_nice(list_results):
    '''
    ### TODO BEGIN ###
    Move that content to the main in downut.py
    ### TODO END ###
    '''
    results_table = rdap_lookup(list_results)
    print('\n', tabulate(results_table, headers=['Main IP','IP Range','ASN','AS Name','Country']))


def rdap_lookup_save_DB(ip_to_check,db_name):
    '''
    ### TODO BEGIN ###
    Under construction
    
    Will save information relative to an IP to the sqlite3 DB
    ### TODO END ###
    '''
    request_tables(db_name,"SELECT * FROM rdap WHERE IP =" + ip_to_check)
    