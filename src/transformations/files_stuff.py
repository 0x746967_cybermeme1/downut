#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  7 12:34:46 2024

@author: cybermime
"""
### IMPORTS ###
import hashlib


### FUCTIONS ###
def file_md5(fname):
    '''
    File hash in MD5

    Return:
        Tuple( name, md5 )
    '''    
    fd = open(fname, 'rb');
    data = fd.read();
    fd.close();

    return (fname, hashlib.md5(data).hexdigest())

