#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 15:57:58 2023

@author: cybermime
"""
### IMPORTS ###
import dns


### CONSTANTES ###
site = ''


### VARIABLES ###
records = [
    'NONE',
    'NULL'
    'ANY',
    'SOA',
    'NS',
    'CAA',
    'MX',
    'A',
    'AAAA',
    'CNAME',
    'TXT',
    'PTR',
    'SRV',
    'MD',
    'MF',
    'MB',
    'MG',
    'MR',
    'WKS',
    'HINFO',
    'MINFO',
    'RP',
    'AFSDB',
    'X25',
    'ISDN',
    'RT',
    'NSAP',
    'NSAP-PTR',
    'SIG',
    'KEY',
    'PX',
    'GPOS',
    'LOC',
    'NXT',
    'NAPTR',
    'KX',
    'A6',
    'DNAME',
    'OPT',
    'APL',
    'DS',
    'SSHFP',
    'IPSECKEY',
    'RRSIG',
    'NSEC',
    'DNSKEY',
    'DHCID',
    'NSEC3',
    'NSEC3PARAM',
    'TLSA',
    'HIP',
    'CDS',
    'CDNSKEY',
    'CSYNC',
    'SPF',
    'UNSPEC',
    'EUI48',
    'EUI64',
    'TKEY',
    'TSIG',
    'IXFR',
    'AXFR',
    'MAILB',
    'MAILA',
    'URI',
    'TA',
    'DLV',
]


### FUNCTIONS ###
def dns_check(url):
    ''' 
    Collect DNS details
    
    Return:
        List
    '''
    result = []
    for val in records:
        try:
            for ipval in dns.resolver.resolve(url, val):
                result.append([url,val,ipval.to_text()])
        
        except:
            pass
    
    return result
