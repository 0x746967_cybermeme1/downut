#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 16 15:47:01 2022

@author: cybermeme

"""
### IMPORTS ###
import subprocess


### FUNCTIONS ###
def nettoy(liste):
    ''' 
    
    Return:
        List
    '''
    for i in range(len(liste)-1):
        liste[i] = liste[i].strip(' ')
    
    return liste
        

def mainipv4_ipv6(domain):
    ''' 
    Extract the main IPs
    
    Return:
        List
    '''
    # Initialize Nslookup
    main_ip4 = main_ip6 = ''
    alias = domain
    result1 = subprocess.run(['host', alias], stdout=subprocess.PIPE)
    
    for lines in str(result1.stdout.decode()).split('\n'):
        if ' is an alias for ' in lines:
            alias = lines.lstrip(alias + ' is an alias for ')
        
        if ' has address ' in lines:
            main_ip4 = lines.strip(alias + ' has address ')
        
        if ' has IPv6 address ' in lines:
            main_ip6 = lines.strip(alias + ' has IPv6 address ')
    
    return [main_ip4,main_ip6]


def IP2AS(domain):
    '''
    Collect the main IPs
    
    Return:
        List
    '''
    main_ipv4, main_ipv6 = mainipv4_ipv6(domain)  
    mainsips = []
    if main_ipv4:
        result_v4 = subprocess.run(['whois', '-h', 'whois.cymru.com', main_ipv4], stdout=subprocess.PIPE)
        mainsips += [nettoy(str(result_v4.stdout.decode()).split('\n')[1].split('|'))]
    
    if main_ipv6:
        result_v6 = subprocess.run(['whois', '-h', 'whois.cymru.com', main_ipv6], stdout=subprocess.PIPE)
        mainsips += [nettoy(str(result_v6.stdout.decode()).split('\n')[1].split('|'))]
    
    return mainsips
