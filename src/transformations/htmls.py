#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 16 20:10:40 2023

@author: cybermime
"""
### IMPORTS ###
from bs4 import BeautifulSoup, Comment


### FUNCTIONS ###
def comments_page(html_doc):
    '''
    extract web pages comments
    
    return list
    '''
    soup = BeautifulSoup(html_doc, 'html.parser')
    list_comments = []
    for comment in soup.findAll(string=lambda text:isinstance(text, Comment)):
        list_comments = list_comments + [comment.strip()]
    return list_comments


def html_page(html_doc):
    '''
    extract webpages links from <a> tags
    
    return list
    '''
    soup = BeautifulSoup(html_doc, 'html.parser')
    list_herf = []
    for a in soup.find_all('a', href=True):
        list_herf = list_herf + [a['href']]
    return list_herf
