# -*- coding: utf-8 -*-
"""
This is the (unofficial) Python API for crt.sh website.
https://crt.sh/


Using this code, you can retrieve subdomains.

need command to be run from the shell: host, whois
dnsutils package if arch linux
"""
### IMPORTS ###
import requests
import json
import time
import os
import socket
import subprocess
import datetime
import ssl


### VARIABLES ###
### TODO BEGIN ###
# Move those variables to argparse
verbose = False
sublister = False

recherche_expires = True #Check expired certificates?
recherche_non_expires = True #Check non expired certificates?
delai_recherche = 2 #increase if lots of certs (delay between requestes)
### TODO END ###


### FUNCTIONS ###
def nettoy(liste):
    ''' 
    Remove extra spaces
    
    Return:
        List
    '''
    for i in range(len(liste)-1):
        liste[i] = liste[i].strip(' ')
    return liste
        

def mainipv4_ipv6(domain):
    ''' 
    Function that retreive main IP
    
    Return:
        List [ipv4, ipv6]
    '''
    main_ip4 = main_ip6 = ''
    alias = domain
    result1 = subprocess.run(['host', domain], stdout=subprocess.PIPE)
    for lines in str(result1.stdout.decode()).split('\n'):
        if ' is an alias for ' in lines:
            alias = lines.lstrip(alias + ' is an alias for ')
        if ' has address ' in lines:
            main_ip4 = lines.strip(domain + ' has address ')
        if ' has IPv6 address ' in lines:
            main_ip6 = lines.strip(domain + ' has IPv6 address ')
    return [main_ip4,main_ip6]


def IP2AS(domain):
    '''
    Extract mains IPs from a Domain
    
    Return:
        List
    '''
    main_ipv4, main_ipv6 = mainipv4_ipv6(domain)  
    mainsips = []
    if main_ipv4:
        result_v4 = subprocess.run(['whois', '-h', 'whois.cymru.com', main_ipv4], stdout=subprocess.PIPE)
        mainsips += [nettoy(str(result_v4.stdout.decode()).split('\n')[3].split('|'))]
    if main_ipv6:
        result_v6 = subprocess.run(['whois', '-h', 'whois.cymru.com', main_ipv6], stdout=subprocess.PIPE)
        mainsips += [nettoy(str(result_v6.stdout.decode()).split('\n')[3].split('|'))]
    return mainsips
    
#fnuctions pour les certifs:
def datetime_to_datetuple(date):
    ''' 
    
    Return:
        Tuple
    '''
    now_to_compare_list = str(date).split()[0].split('-')
    return (datetime.date(int(now_to_compare_list[0]), int(now_to_compare_list[1]), int(now_to_compare_list[2])))


def get_cert(hostname, port):
    ''' 
    This function return the certificate
    
    Return: 
        Dictionary
    '''
    #by default the port is 443, if the port is different, call the function with that 2nd argument
    context = ssl.create_default_context()
    with socket.create_connection((hostname, port)) as sock:
        with context.wrap_socket(sock, server_hostname=hostname) as ssock:
            dico = ssock.getpeercert()
            return(dico)


def get_cert_date(dico):
    ''' 
    This function return the date of expiration
    
    Return: 
        Timestamp
    '''
    date = dico['notAfter']
    datetime_object = datetime.datetime.strptime(date, '%b %d %H:%M:%S %Y %Z')
    return(datetime_object)


def get_cert_CA(dico):
    ''' 
    This function return the name of the Certificate authority
    
    Return: 
        String
    '''
    return(dico['issuer'][1][0][1])


def certificates_informations(now_to_compare, domain='', port=443):
    '''
    Function that check if the certificate is still valid
    '''
    try:
        dico = get_cert(domain, port)
        #Getting the expiration date#
        result_cert = datetime_to_datetuple(get_cert_date(dico))
        #Getting the certificate authority#
        print('[@] Certificat edite par : ', get_cert_CA(dico))
        #Calculation of the remaning number of days#
        jours_restants = (result_cert - now_to_compare).days
        if jours_restants > 0:
            print('[@] Valide pour : ', jours_restants, 'days')
            
        else:
            print('[!] Certificat out of date - Have a look [!]'.center(50,'!'))
            
    except:
        print('!!!-- ISSUE WITH :', domain, ' --!!!')
        pass


def search(domain, wildcard=True, expired=True):
    """
    Search crt.sh for the given domain.

    domain -- Domain to search for
    expired -- Whether or not to include expired certificates
                (default: True)

    Return a list of objects, like:

    {
        "issuer_ca_id": 16418,
        "issuer_name": "C=US, O=Let's Encrypt, CN=Let's Encrypt Authority X3",
        "name_value": "hatch.uber.com",
        "min_cert_id": 325717795,
        "min_entry_timestamp": "2018-02-08T16:47:39.089",
        "not_before": "2018-02-08T15:47:39"
    }
    """
    base_url = "https://crt.sh/?q={}&output=json"
    if not expired:
        base_url = base_url + "&exclude=expired"
    url = base_url.format(domain)

    ua = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
    req = requests.get(url, headers={'User-Agent': ua})

    if req.ok:
        try:
            content = req.content.decode('utf-8')
            data = json.loads(content)
            return data
        
        except ValueError:
            # crt.sh fixed their JSON response. This shouldn't be necessary anymore
            # https://github.com/crtsh/certwatch_db/commit/f4f46ea37c23543c4cdf1a3c8867d68967641807
            data = json.loads("[{}]".format(content.replace('}{', '},{')))
            return data
        
        except Exception as err:
            print("Error retrieving information." + err)
            
    return None


def clean_name_value(data, list_to_check):
    '''
    function getting the list of domains used by a certificate and
    transform it in to a list and remove the wildcards
    print in to the terminal the wildcards to do a manual check
    
    Return: 
        List
    '''
    list_full = []
    list_cleaned = []
    list_full += data.split('\n')
    for domains in list_full:
        if domains.startswith('*'):
            print('[!] Warning, there is a wildcard: ' + domains)
        #print(domains) removing the already in the list and the wildcard
        
        if domains not in list_to_check and not domains.startswith('*'):
            list_cleaned = list_cleaned + [domains]
            
    return list_cleaned


def recherche_domains_certs(expire_ou_non, delai_recherche, list_domains):
    '''
    Search for target domains in certificates

    Return: 
        List
    '''
    recup_domains = []
    for line in list_domains:
        time.sleep(delai_recherche)
        domain_list = search(line.strip('\n'),expired=expire_ou_non)
        
        for list_dom in domain_list:
            try:
                if list_dom['name_value'] not in recup_domains:
                    recup_domains = recup_domains + clean_name_value(list_dom['name_value'], recup_domains)
                    
            except:
                continue
            
    return recup_domains


def ping(domain):
    '''
    Try to ping the target
    
    Return:
        Boolean
    '''
    if check_localhost(domain):
        return False
    
    if verbose:
        print ('[!] Trying: ',domain, ' - ICMP')
    req = os.system("ping -c 1 -w 1 " + domain + " > /dev/null 2>&1") #send 1 1 second TTL
    
    if req == 0:
        if verbose:
            print (5*' ' + '[o/] ICMP - ok')
        return True
    else:
        return False


def tcping(domain,ports):
    '''
    Try to TCP scan the target on a defined ports range
    
    Return:
        Boolean
    '''
    for PORT in ports:
        if verbose:
            print ('[!] Trying: ',domain, ' - Port: ', PORT)
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect((domain, PORT))
            if verbose:
                print (5*' ' + '[o/] TCP - ',domain, ' - Port: ', PORT, ' - ok')

            return True

        except:
            continue
        
    return False


def sublit3r(domain):
    '''
    Use sublist3r tool to bruteforce subdomains
    
    Return:
        List
    '''
    print('[!] Sublister provide different results on every check, it is not reliable'.center(80,'_'))
    liste_domains = []
    if verbose:
        print ('[!] sublit3r: ',domain)
        
    req = subprocess.run(['/usr/bin/sublist3r', '-n', '-d', domain], stdout=subprocess.PIPE)
    for lines in str(req.stdout.decode()).split('\n'):
        if domain in lines:
            liste_domains += [lines] 
        else:
            continue
        
    if liste_domains[1:]:
        return liste_domains[1:]


def check_localhost(domain):
    '''
    Check if localhost in a dns record (used to slow down automated scanning tools)
    
    Return:
        Boolean
    '''
    # Initialize Nslookup
    result1 = subprocess.run(['host', domain], stdout=subprocess.PIPE)
    for lines in str(result1.stdout.decode()).split('\n'):
        if '127.0.0.1' in lines:
            return True
        
    return False


def cert_check_hunt(list_domains):
    '''
    Extract certificates and run all the tests
    '''
    #collect data
    list_everything = []
    
    domains_valids = recherche_domains_certs(recherche_non_expires, delai_recherche, list_domains)
    domains_valids_and_not = recherche_domains_certs(recherche_expires, delai_recherche, list_domains)
    
    list_everything += domains_valids_and_not

    if sublister:
        for i in list_domains:
            try:
                list_everything = domains_valids_and_not + sublit3r(i)
            except:
                continue
    
    # Extraction of domains by compare lists with and without validity
    domains_out_of_date = []
    for i in domains_valids_and_not:
        if i not in domains_valids:
            domains_out_of_date += [i]
            if verbose: 
                print('[!] Domains out of date:',i)
    
    # check if domains are up (if m$ server, ICMP is block by default)
    final_domains_up = []
    final_domains_down = []
    
    for domain in list_everything:
        print(domain.center(50,'*'))
        certificates_informations(now_to_compare, domain)
        if not ping(domain):
            continue
            ### TODO BEGIN ###
            # Restore ping if not cloudflare and akamai
            #if not tcping(domain,ports_a_check):
            #    final_domains_down += [domain]
            #else:
            #    final_domains_up += [domain]
            ### TODO END ###
        else:
            final_domains_up += [domain]

    # Print results
    for domain in final_domains_down:
        print('[-] Ping ', domain, ' seems down')
        
    for domain in final_domains_up:
        print('\n', 5* ' ' + '[+] Ping ', domain, ' UP')


### VARIABLE Part 2 ###
now_to_compare = datetime_to_datetuple(datetime.datetime.now())
