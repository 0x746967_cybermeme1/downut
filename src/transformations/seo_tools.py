#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 17:11:39 2023

@author: cybermime

"""
### IMPORT ###
from tabulate import tabulate
import requests


### VARIABES ###
result = {}


### FUNCTIONS ###
def robot_hunting(fqdn, user_agent):
    ''' 
    Connect to the target and try to download robots.txt file
    
    Return:
        Dictonnary
    '''
    user_agent = {'User-agent': user_agent }
    try:
        robot_txt = fqdn + '/robots.txt'
        r = requests.get(robot_txt, headers=user_agent)
        raw_return = r.text.split('\n')
       
        for i in raw_return:
            if ':' in i:
                cle = i.split(':')[0]
                value = ':'.join(i.split(':')[1:])
                result[cle] = value
                
    except:
        print("Error while collecting robots.txt")
        
    return result


def pretify_robot_result(dico, fqdn, print_tab=False):
    '''
    Pretify the robots.txt results

    Return:
        List
    '''
    liste_temp = []
    liste_disal = []
    url_temp = ""
    for key in dico.keys():
        #on ajoute le fqdn si besoin
        if dico[key].lstrip().startswith('/'):
            url_temp = fqdn+dico[key].lstrip()
        else:
            url_temp = dico[key].lstrip()
        liste_temp.append([key,url_temp])
        
        if key == 'Disallow':
            liste_disal.append(url_temp)
            
    if print_tab:
        print('\n', tabulate(liste_temp, headers=['key','Value']))
        
    return liste_disal


def sitemap_hunting(fqdn):
    '''
    Function that will parse sitemap.xml file
    '''
    ### TODO ###
    pass


def security_txt_hunting(fqdn):
    '''
    Function that will parse ./well-known/security.txt file
    '''
    ### TODO ###
    pass
