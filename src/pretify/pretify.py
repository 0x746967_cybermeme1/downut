#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 22:23:50 2024

@author: cybermime
"""

### FUNCTIONS ###
def line_separator():
    ''' 
    Print a line separator
    '''
    print('\n'*3 + "==="+"%<"*30+ "===" + '\n'*2) 