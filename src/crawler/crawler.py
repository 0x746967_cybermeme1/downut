#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 14:22:30 2023

@author: cybermime

"""

### IMPORTS ###
import jsbeautifier
import requests
import time

from ..transformations.files_stuff import file_md5

from bs4 import BeautifulSoup
from bs4 import Comment
from random import randrange
#from tabulate import tabulate
from tabulate import SEPARATING_LINE


### CONSTANTES ###
XSS_DOM_SINKS = ['document.write()',
'document.writeln()',
'document.domain',
'element.innerHTML',
'element.outerHTML',
'element.insertAdjacentHTML',
'element.onevent']

XSS_DOM_JQUERY_SINKS = ['add()',
'after()',
'append()',
'animate()',
'insertAfter()',
'insertBefore()',
'before()',
'html()',
'prepend()',
'replaceAll()',
'replaceWith()',
'wrap()',
'wrapInner()',
'wrapAll()',
'has()',
'constructor()',
'init()',
'index()',
'jQuery.parseHTML()',
'$.parseHTML()']


# initialize the list of discovered urls
# with the first page to visit
urls = []

js_list = set()
liste_comments_js_parsed = set()


### FUNCTIONS ###
def parse_comments(soup, current_url, result):
    #Adding comments, one / line
    comments = soup.find_all(string=lambda text: isinstance(text, Comment))
    i = 0
    for c in comments:
        liste_tampon = [current_url]
        i += 1
        liste_tampon.append(str(i))
        liste_tampon.append(c)
        result.append(liste_tampon)
        if c == comments[-1]:
            result.append(SEPARATING_LINE)


def parse_href(soup, current_url, already_checked, urls):
    link_elements = soup.select("a[href]")
    for link_element in link_elements:
        url = link_element['href']
        doublon = False
        # if internal link, -> absolute link
        if url.startswith('./'):
            url = url.replace('./', current_url.rstrip('/') + '/')
        
        # if already processes -> pass
        if current_url == url or current_url+'/' == url:
            pass
        
        # if already checked -> marked as already done
        for i in already_checked:
            if i == url or i+'/' == url:
                doublon = True
        
        if not doublon:
            if current_url in url:
                urls.append(url)
        

def parse_form_csrf(soup):
    '''
    Check for inline scripts
    '''
    script_elements = soup.select("script")
    for script_element in script_elements:
        for i in XSS_DOM_SINKS:
            if i in script_element:
                print("XSS DOM possible Thanks to: {} in:\n{}".format(i,script_element))
        
        for i in XSS_DOM_JQUERY_SINKS:
            if i in script_element:
                print("XSS JQUERY DOM possible Thanks to: {} in:\n{}".format(i,script_element))


def parse_js(soup, current_url, already_checked, urls):
    '''
    Extract js files
    '''
    link_elements = soup.select("script[src]")
    for link_element in link_elements:
        url = link_element['src']
        if url.endswith('.js'):
            # if internal link, -> absolute link
            if url.startswith('./'):
                url = url.replace('./', current_url.rstrip('/') + '/')
            
            if url.startswith('//'):
                url = 'https:' + url
            
            # if already processed -> pass
            if current_url == url or current_url+'/' == url:
                pass
            
            #Using set() type of list we don't bother if already added \o/
            js_list.add(url)

    
def parse_js_comment(file_url, fqdn, user_agent, save_file, print_comments):
    '''
    Parse comments from JS file
    '''
    user_agent = {'User-agent': user_agent }
    
    global liste_comments_js_parsed
    file_url_origin = file_url
    
    if not file_url in liste_comments_js_parsed:
        # Check if the name starts with / -> add fqdn before
        if file_url.startswith('/'):
            file_url = fqdn + file_url
        
        response = requests.get(file_url, headers = user_agent)
        
        # Backup original file
        if save_file:   
            fd_oring = open(save_file+'_orig', 'w')
            fd_oring.write(response.text)
            fd_oring.close()
            print(file_md5(save_file+'_orig'))
        
        lines = response.text.split('\n')
        count_lines = 0
        
        if save_file:
            fd = open(save_file, 'a')
            fd.write("\t[+] File parse Begin: {}\t[+]\n".format(file_url))
        
        print("\t[+] File parse: {}\t[+]".format(file_url))
        for line in lines:
            res = jsbeautifier.beautify(line)
            for l in res.split('\n'):
                count_lines += 1
                #if '//' in l or '/*' in l or '*/' in l:
                    #print("{} - line {} : {}".format(file_url,count_lines ,l))
                if save_file:
                    fd.write("line {} : {}\n".format(count_lines ,l))
        
                if print_comments:
                    print("line {} : {}".format(count_lines ,l))
        if save_file:
            fd.write("\t[-] File parse End: {}\t[-]\n".format(file_url))
            fd.close()
        
        if print_comments:
            print("\t[-] File parse End: {}\t[-]\n\n".format(file_url))
    
        # Adding to to not parse list
        liste_comments_js_parsed.add(file_url_origin)
        print(file_url_origin, 'Added')
    

def parse_xss_dom(soup, cookie=None):
    script_elements = soup.select("form")
    print(script_elements)


def crawl_comments(urls, delay, ua, save_file, print_comments):
    ''' 
    Extract comments
    Return List
    '''
    user_agent = {'User-agent': ua }
    already_checked = []
    result = []
    
    # until all pages have been visited
    while len(urls) != 0:
        # get the page to visit from the list
        current_url = urls.pop()
        
        # random time delay between requests
        random_time = randrange(delay)/1000
        time.sleep(random_time)
        
        # crawling logic
        try:
            response = requests.get(current_url, headers = user_agent)
            soup = BeautifulSoup(response.content, "html.parser")
        
            already_checked.append(current_url)
        except:
            print("Erreur de conexion du parseur")
            
        # Parsing for comments
        parse_comments(soup, current_url, result)
    
        # Hunting links
        parse_href(soup, current_url, already_checked, urls)
        parse_js(soup, current_url, already_checked, urls)

        # Parse for JS comments
        for file_url in js_list:
            parse_js_comment(file_url, current_url, ua, save_file, print_comments)

        #XSS DOM search
        ### TODO BEGIN ###
        # DEBUG
        parse_form_csrf(soup)
        ### TODO END ###

    return result
