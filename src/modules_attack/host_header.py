#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 20:03:28 2023

@author: cybermime

Remind to get hexadigit:
    import string
    list_hexdigit = string.ascii_letters + string.digits
    

TODO:
    UNDER DEVELOPMENT

"""
### IMPORTS ###
import hashlib
import requests
import time

from random import randrange
from tabulate import tabulate
from tabulate import SEPARATING_LINE

from ..transformations.domain import get_domain_name,get_sub_domain_name


### VARIABLES ###
list_of_results = []

list_of_header_to_remove = ["date"]

USER_AGENTS = ['Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0','','<scRiPt>print(1)<scRipT>','Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html)','\00']


def prepare_headers(url, subdomains, fqdn, bad_stuff_2_inject, subdomain_hijack, USER_AGENTS):
    '''
    Prepare the list of headers for injection
    
    Return:
        List
    '''
    hosts_list_to_test = []
    for ua in USER_AGENTS:
        #retourne la liste de header comme il faut
        hosts_list_to_test = hosts_list_to_test + [
            {"User-Agent":ua,"Host":subdomains},
            {"User-Agent":ua,"Host":bad_stuff_2_inject},
            {"User-Agent":ua,"Host": subdomain_hijack+'.'+fqdn} ,
            {"User-Agent":ua,"Host":bad_stuff_2_inject,"%09Host":fqdn}, 
            {"User-Agent":ua,"Host":bad_stuff_2_inject,"%48ost":fqdn},
            {"User-Agent":ua,"X-Forwarded-Host":bad_stuff_2_inject},
            {"User-Agent":ua,"X-Host":bad_stuff_2_inject},
            {"User-Agent":ua,"X-Forwarded-Server": bad_stuff_2_inject},
            {"User-Agent":ua,"X-HTTP-Host-Override":bad_stuff_2_inject},
            {"User-Agent":ua,"Forwarded":bad_stuff_2_inject}]
    return hosts_list_to_test


def de_date_header(headers):
    '''
    Remove Date from the headers
    
    Return:
        List
    '''
    final_header = {}
    for i in headers.keys():
        if i.lower() not in list_of_header_to_remove:
            final_header[i] = headers[i]
    return final_header
            

def check_if_multiple_results(set_to_check, list_of_results):
    '''
    Check if results differs between original header and forged one
    
    Return:
        Tuple
    '''
    #In set() we trust
    result_difer = set()
    result_difer_fqdn = set()
    
    if len(set_to_check) > 1:
        print('Differents results:')
        print('===================')
        for hash_found in list_of_results:
            if len(hash_found) == 4:
                if hash_found[2] in set_to_check:
                    result_difer_fqdn.add(hash_found[0])
                    result_difer.add(hash_found[2])
                
                if  hash_found[3] in set_to_check:
                    result_difer_fqdn.add(hash_found[0])
                    result_difer.add(hash_found[3])
        
        return (result_difer_fqdn, result_difer)


def main_host_header_injection(urls, bad_stuff_2_inject, subdomain_hijack, ua, bourrin_user_agent=False, delay_en_ms=2000):
    '''
    Inject header for csrf
    
    Return:
        Tuple(List , tuple)
    '''
    headers_printed = False
    
    for url in urls:
        # Set a random time between 2 requests
        random_time = randrange(delay_en_ms)/1000
        time.sleep(random_time)
        
        single_body_hash = set()
        single_header_hash = set()
        fqdn = get_domain_name(url)
        subdom = get_sub_domain_name(url)
        if not bourrin_user_agent:
            user_agents = [ua]
        
        hosts_list_to_test = prepare_headers(url, subdom, fqdn, bad_stuff_2_inject, subdomain_hijack, user_agents)
        for host_tested in hosts_list_to_test:
            if not headers_printed:
                print(host_tested)
                headers_printed = True
            
            results = requests.get(url, headers=host_tested)
            result_hash_body = hashlib.md5(results.text.encode()).hexdigest()
            result_header = de_date_header(results.headers)
            result_hash_header = hashlib.md5(str(result_header).encode()).hexdigest()
            
            single_body_hash.add(result_hash_body)
            single_header_hash.add(result_hash_header)
            
            list_of_results.append([ url, host_tested, result_hash_body, result_hash_header])
            
        list_of_results.append(SEPARATING_LINE)
        liste_doublon = check_if_multiple_results(single_body_hash, list_of_results)
        
        return (list_of_results, liste_doublon)


### MAIN FOR TESTS ###
if __name__ == "__main__":
    # if bourrin, send plenty of differents user agents (list provided in constants)
    bourin = False
    urls = ["http://127.0.0.1:8888"]
    results, doublons = main_host_header_injection(urls, "127.0.0.1", "pwned", "hacker_1")

    print('Host header Injection:')
    print('======================')
    print('\n', tabulate(results, headers=['URL','Hosts','Body', "Headers"]))
    if doublons:
        print("Doublons Url: {} - md5 {}".format(doublons[0],doublons[1]))

    